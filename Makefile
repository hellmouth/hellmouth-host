# This file is part of the HELLMOUTH cluster system.
# Copyright 2024 Graham Shaw.
# Distribution and modification are permitted within the terms of the
# GNU General Public License (version 3 or any later version).

prefix = /usr/local
exec_prefix = $(prefix)
libdir = $(exec_prefix)/lib
libexecdir = $(libdir)
includedir = $(exec_prefix)/include
datadir = /var/lib

pkgname = hellmouth

CPPFLAGS = -MD -MP -I. '-DLIBEXECDIR="$(libexecdir)"' '-DDATADIR="$(datadir)"' '-DPKGNAME="$(pkgname)"'
CXXFLAGS = -fPIC -O2 --std=c++2a -Wall -Wpedantic
LDLIBS = -latomic -lsodium $(libdir)/hellmouth.so

SRC = $(wildcard src/*.cc)
AUXBIN = $(SRC:src/%.cc=bin/%)

DRVDIRS = $(wildcard drivers/*/*)
DRVLIBS = $(foreach DRVDIR,$(DRVDIRS),$(DRVDIR)/$(notdir $(DRVDIR)).so)

HELLMOUTH = $(wildcard hellmouth/*.cc) $(wildcard hellmouth/*/*.cc)
INCLUDE = $(wildcard hellmouth/*.h) $(wildcard hellmouth/*/*.h)

.PHONY: all
all: hellmouth-host.so $(AUXBIN) $(DRVLIBS)

hellmouth-host.so: $(HELLMOUTH:%.cc=%.o)
	gcc -shared -o $@ $^ $(LDLIBS)

$(AUXBIN): bin/%: src/%.o hellmouth-host.so
	@mkdir -p bin
	g++ -rdynamic -Wl,-rpath $(libdir) -o $@ $^ $(LDLIBS)

drivers/%.so: always
	make -C $(dir $@)

.PHONY: clean
clean: $(DRVDIRS:%=%/clean)
	rm -f hellmouth/*.[do]
	rm -f hellmouth/*/*.[do]
	rm -f src/*.[do]
	rm -f *.so
	rm -rf bin

%/clean: always
	make -C $* clean

.PHONY: install
install: $(INCLUDE:%.h=$(includedir)/%.h) $(DRVDIRS:%=%/install)
	@mkdir -p $(libexecdir)/$(pkgname)/bin
	@mkdir -p $(libdir)
	cp $(AUXBIN) $(libexecdir)/$(pkgname)/bin/
	cp hellmouth-host.so $(libdir)/

$(includedir)/%.h: %.h
	@mkdir -p $(dir $@)
	cp $< $(dir $@)

lastdir = $(notdir $(patsubst %/,%,$(dir $(1))))
drvdest = $(libexecdir)/$(pkgname)/drivers/$(call lastdir,$(1))

%/install: all
	mkdir -p $(DESTDIR)/$(call drvdest,$*)/
	cp $*/*.so $(DESTDIR)/$(call drvdest,$*)/

.PHONY: uninstall
uninstall:
	rm -f $(INCLUDE:%.h=$(includedir)/%.h)
	find $(includedir)/hellmouth -type d -empty -delete
	rm $(libdir)/hellmouth-host.so
	rm -rf $(libexecdir)/$(pkgname)

.PHONY: always
always:

-include $(HELLMOUTH:%.cc=%.d)
-include $(SRC:%.cc=%.d)
