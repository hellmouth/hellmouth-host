# HELLMOUTH Cluster System: Host Daemon

## Purpose

A HELLMOUTH cluster will provide an efficient, container-based,
high-availability computing system.

This repository provides the host daemon responsible for managing
hosts and the resources associated with them.

## Current status

Incomplete and experimental.

## Copyright and licensing

The HELLMOUTH cluster system is copyright 2024 Graham Shaw.

Distribution and modification are permitted within the terms of the GNU
General Public License (version 3 or any later version).

Please refer to the file LICENCE.md for details.

Third-party contributions are not currently being accepted into this
project.
