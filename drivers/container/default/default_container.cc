// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <unistd.h>
#include <sched.h>
#include <sys/stat.h>
#include <sys/mount.h>
#include <sys/syscall.h>

#include "hellmouth/os/utility.h"
#include "hellmouth/os/command.h"
#include "hellmouth/os/lockfile.h"
#include "hellmouth/artifacts/artifact_name.h"
#include "hellmouth/artifacts/image_store.h"
#include "hellmouth/resources/volume.h"

#include "default_container.h"

namespace hellmouth::resources {

static hellmouth::artifacts::image_store& images() {
	static hellmouth::artifacts::image_store images(
		std::filesystem::path(DATADIR) / PKGNAME / "images");
	return images;
}

default_container::default_container(resource* parent,
	resource_name name, spec_type spec):
	container(parent, std::move(name), std::move(spec)),
	_pathname(make_pathname(this->name())) {

	// [TODO]: investigate whether there is an exploitable race
	// condition here (similarly to artifacts::layer_store).
	create_directories(_pathname);
	permissions(_pathname, std::filesystem::perms::owner_all);
}

std::filesystem::path default_container::root_pathname() const {
	std::filesystem::path root_pathname = _pathname / "root";
	create_directory(root_pathname);
	return root_pathname;
}

std::filesystem::path default_container::upper_pathname() const {
	std::filesystem::path root_pathname = _pathname / "upper";
	create_directory(root_pathname);
	return root_pathname;
}

std::filesystem::path default_container::work_pathname() const {
	std::filesystem::path root_pathname = _pathname / "work";
	create_directory(root_pathname);
	return root_pathname;
}

std::filesystem::path default_container::lock_pathname() const {
	std::filesystem::path root_pathname = _pathname / ".lock";
	create_directory(root_pathname);
	return root_pathname;
}

bool default_container::is_mounted() const {
	struct stat dstatbuf;
	struct stat pstatbuf;
	if (stat(root_pathname().c_str(), &dstatbuf) == -1) {
		throw_errno();
	}
	if (stat(root_pathname().parent_path().c_str(), &pstatbuf) == -1) {
		throw_errno();
	}
	return dstatbuf.st_dev != pstatbuf.st_dev;
}

void default_container::mount_root() {
	// Acquire the lockfile.
	os::lockfile lock(make_lockname(_pathname));

	// No action required if the container is already mounted.
	if (is_mounted()) {
		return;
	}

	// Pull image.
	hellmouth::artifacts::artifact_name imgname(spec().at("image"));
	imgname = imgname.canonicalise();
	bool secure = spec().at("secure");
	images()[imgname].pull(secure);

	// Construct colon-separated list of pathnames for the required
	// lower layers.
	auto layers = images()[imgname].layers();
	std::string lower_pathnames;
	for (auto& layer: layers) {
		if (!lower_pathnames.empty()) {
			lower_pathnames.append(":");
		}
		lower_pathnames.append(layer);
	}

	// Construct option string.
	std::string options;
	options.append("lowerdir=")
		.append(lower_pathnames)
		.append(",upperdir=")
		.append(upper_pathname())
		.append(",workdir=")
		.append(work_pathname());

	// Mount container root.
	os::command cmd("/usr/bin/mount");
	cmd.append("-t").append("overlay")
		.append("overlay")
		.append("-o").append(options)
		.append(root_pathname());

	if (cmd.exec()) {
		throw mount_error(
			std::string("failed to mount container ") + name());
	}
}

void default_container::mount_volumes() {
	if (spec().contains("mount")) {
		for (const auto& [mountpoint, mountspec] : to_object(spec().at("mount"))) {
			std::filesystem::path internal_mountpoint = to_string(mountpoint);
			std::filesystem::path external_mountpoint =
				root_pathname() / internal_mountpoint.relative_path();
			create_directories(external_mountpoint);
			auto& vol = find<volume>(mountspec.at("volume"));
			vol.mount(external_mountpoint, mountspec);
		}
	}
}

void default_container::mount() {
	mount_root();
	mount_volumes();
}

void default_container::enter() {
	std::filesystem::path oldroot = "oldroot";
	std::filesystem::path external_newroot = root_pathname();
	std::filesystem::path external_oldroot = external_newroot / oldroot;
	std::filesystem::path internal_newroot = "/";
	std::filesystem::path internal_oldroot = internal_newroot / oldroot;
	create_directory(external_oldroot);

	// Create and enter a new mount namespace.
	if (::unshare(CLONE_NEWNS) == -1) {
		throw_errno();
	}

	// Change the propagation mode of all mount points within the
	// newly-created namespace, so that they can be unmounted without
	// affecting other namespaces.
	if (::mount(0, internal_newroot.c_str(), 0,
		MS_PRIVATE | MS_REC, 0) == -1) {

		throw_errno();
	}

	// Pivot to the required new root directory.
	int retval = syscall(SYS_pivot_root,
		external_newroot.c_str(), external_oldroot.c_str());
	if (retval == -1) {
		throw_errno();
	}

	// Change the current working directory to the new root directory.
	if (::chdir(internal_newroot.c_str()) == -1) {
		throw_errno();
	}

	// Umount the old root directory, and everything beneath it.
	if (::umount2(internal_oldroot.c_str(), MNT_DETACH) == -1) {
		throw_errno();
	}

	remove(internal_oldroot);
}

void default_container::exec() {
	std::filesystem::path pathname =
		to_string(spec().at("command"));
	os::command cmd(pathname);
	if (spec().contains("arguments")) {
		for (const auto& arg : to_array(spec().at("arguments"))) {
			cmd.append(arg);
		}
	}
	cmd.exec(false);
}

os::process_descriptor default_container::fork() {
	pid_t pid = ::fork();
	if (pid == -1) {
		throw_errno();
	} else if (pid == 0) {
		enter();
		exec();
		_exit(127);
	}
	return os::process_descriptor(pid, 0);
}

int default_container::pollfd() const {
	return _pidfd;
}

void default_container::start() {
	mount();
	_pidfd = fork();
	state(state_type::running);
}

void default_container::kill(int sig) {
	_pidfd.kill(sig);
	switch (sig) {
	case SIGTERM:
		if (state() == state_type::running) {
			state(state_type::terminating);
		}
		break;
	case SIGKILL:
		if (state() == state_type::running ||
			state() == state_type::terminating) {

			state(state_type::killing);
		}
		break;
	}
}

siginfo_t default_container::wait() {
	siginfo_t info = _pidfd.wait(WEXITED);
	state(state_type::idle);
	return info;
}

}

extern "C"
std::unique_ptr<hellmouth::resources::resource> driver(
	hellmouth::resources::resource* parent,
	hellmouth::resources::resource_name name,
	hellmouth::resources::resource::spec_type spec) {

	return std::make_unique<hellmouth::resources::default_container>(
		parent, std::move(name), std::move(spec));
}
