// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DRIVER_CONTAINER_DEFAULT_CONTAINER
#define HELLMOUTH_DRIVER_CONTAINER_DEFAULT_CONTAINER

#include "hellmouth/os/process_descriptor.h"
#include "hellmouth/resources/container.h"

namespace hellmouth::resources {

/** The default container implementation.
 * The container root is mounted using OverlayFS.
 *
 * Currently the container is isolated only by means of a mount namespace,
 * but this is not expected to remain the case and the default behaviour
 * is likely to change.
 */
class default_container:
	public container {
private:
	os::process_descriptor _pidfd;

	/** The pathname for storing data associated with this container. */
	std::filesystem::path _pathname;

	/** Get root pathname.
	 * @return the pathname for mounting the root of this container.
	 */
	std::filesystem::path root_pathname() const;

	/** Get upper layer pathname.
	 * @return the pathname for the upper layer of the overlay mount.
	 */
	std::filesystem::path upper_pathname() const;

	/** Get work dorectory pathname.
	 * @return the pathname for the working directory of the overlay mount.
	 */
	std::filesystem::path work_pathname() const;

	/** Get lockfile pathname.
	 * @return the pathname of the lockfile.
	 */
	std::filesystem::path lock_pathname() const;

	/** Determine whether the root of this container is mounted.
	 * A default implementation is provided which is capable of
	 * distinguishing between the following two conditions:
	 *
	 * - There is nothing mounted at the container root.
	 * - There is a single filesystem mounted at the container root.
	 *
	 * This is done by testing whether the container root and its
	 * parent directory have the same device number, which works
	 * when a filesystem is mounted, but not for bind mounts.
	 *
	 * @return true if root is mounted, otherwise false
	 */
	bool is_mounted() const;

	/** Mount the root filesystem for this container. */
	void mount_root();

	/** Mount all volumes for this container.
	 * The root filesystem must already have been mounted.
	 */
	void mount_volumes();

	/** Mount this container, includig the root filesystem and all volumes.
	 * If the container is already mounted then calling this function has
	 * no effect.
	 */
	void mount();

	/** Unmount this container.
	 * If the container is already unmounted then calling this function has
	 * no effect.
	 */
	void umount();

	/** Create and enter a mount namespace for this container.
	 * The container root must be a mount point (which will be the case if
	 * if the mount function has been called).
	 */
	void enter();

	/** Execute command. */
	void exec();

	/** Fork subprocess and execute command. */
	os::process_descriptor fork();
public:
	/** Construct container.
	 * @param parent of this container, or 0 if none
	 * @param name the name of this container
	 * @param spec the specification of this container
	 */
	default_container(resource* parent, resource_name name, spec_type spec);

	int pollfd() const override;
	void start() override;
	void kill(int sig) override;
	siginfo_t wait() override;
};

}

#endif
