// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <fcntl.h>

#include "hellmouth/os/file_descriptor.h"
#include "hellmouth/artifacts/algorithm.h"

namespace hellmouth::artifacts {

void algorithm::read(const std::string& pathname) {
	os::file_descriptor fd(pathname, O_RDONLY);
	char buffer[0x1000];
	while (size_t count = fd.read(buffer, sizeof(buffer))) {
		(*this)(buffer, count);
	}
}

}
