// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ARTIFACTS_ALGORITHM
#define HELLMOUTH_ARTIFACTS_ALGORITHM

#include "hellmouth/artifacts/digest.h"

namespace hellmouth::artifacts {

/** An abstract base class to represent a digest algorithm. */
class algorithm {
public:
	/** Incorporate raw data into digest.
	 * @param data the data to be incorporated
	 * @param length the length of the data, in octets
	 */
	virtual void operator()(const void* data, size_t length) = 0;

	/** Finalise and return digest.
	 * @return the resulting digest
	 */
	virtual digest operator()() = 0;

	/** Get the name of this algorithm.
	 * @return the algorithm name
	 */
	virtual const algorithm_name& name() const = 0;

	/** Incorporate content of file.
	 * @param pathname the pathname of the file to be incorpoated
	 */
	void read(const std::string& pathname);
};

}

#endif
