// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>
#include <format>
#include <regex>

#include "hellmouth/artifacts/algorithm_name.h"
#include "hellmouth/artifacts/sha256.h"
#include "hellmouth/artifacts/sha512.h"

namespace hellmouth::artifacts {

algorithm_name::algorithm_name(std::string_view name):
	_name(name) {

	static const std::regex repository_pattern(
		std::format("{0}({1}{0})*",
		R"([a-z0-9]+)",
		R"([+._-])"));

	if (!std::regex_match(_name, repository_pattern)) {
		throw std::invalid_argument("invalid algorithm name");
	}
}

std::unique_ptr<algorithm> make_algorithm(const algorithm_name& algname) {
	typedef std::unique_ptr<algorithm> algorithm_ptr;
	typedef std::function<algorithm_ptr()> algorithm_maker;
	static std::map<std::string, algorithm_maker> registered_algorithms = {
		{"sha256", [](){ return std::make_unique<sha256>(); }},
		{"sha512", [](){ return std::make_unique<sha512>(); }}};

	auto f = registered_algorithms.find(algname);
	if (f == registered_algorithms.end()) {
		throw std::out_of_range("unrecognised algorithm name");
	}
	return (f->second)();
}

}
