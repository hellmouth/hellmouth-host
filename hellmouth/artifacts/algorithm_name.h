// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ARTIFACTS_ALGORITHM_NAME
#define HELLMOUTH_ARTIFACTS_ALGORITHM_NAME

#include <memory>
#include <string>
#include <string_view>

namespace hellmouth::artifacts {

class algorithm;

/** A class to represent a validated digest algorithm name.
 * An algorithm name is valid if it matches the following grammar:
 *
 * algorithm ::= algorithm-component (algorithm-separator
 *     algorithm-component)*
 * algorithm-component ::= [a-z0-9]+
 * algorithm-separator ::= [+._-]
 *
 * (Based on OCI image spec v1.1.0, descriptor.md#digests.)
 */
class algorithm_name {
private:
	/** The algorithm name as a string. */
	std::string _name;
public:
	/** Construct algorithm name from character string.
	 * @param value the algorithm name as a std::string_view
	 */
	explicit algorithm_name(std::string_view name);

	/** Get this algorithm name as a string.
	 * @return the algorithm name
	 */
	operator const std::string&() const {
		return _name;
	}

	bool operator==(const algorithm_name&) const = default;
	auto operator<=>(const algorithm_name&) const = default;
};

inline const std::string& to_string(const algorithm_name& algname) {
	return algname;
}

/** Make an instance of the named algorithm.
 * @return the algorithm instance
 */
std::unique_ptr<algorithm> make_algorithm(const algorithm_name& algname);

}

#endif
