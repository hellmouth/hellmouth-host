// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/artifacts/artifact_name.h"

namespace hellmouth::artifacts {

class artifact_name::parser {
private:
	std::optional<std::string> _registry;
	std::string _repository;
	char _prefix;
	std::string _reference;
public:
	parser(std::string unparsed);
	std::optional<registry_name> registry() const {
		if (_registry) {
			return registry_name(*_registry);
		} else {
			return std::optional<registry_name>();
		}
	}
	repository_name repository() const {
		return repository_name(_repository);
	}
	repository_reference reference() const {
		if (_prefix != 0) {
			return repository_reference(_prefix, _reference);
		} else {
			return repository_reference();
		}
	}
};

artifact_name::parser::parser(std::string unparsed) {
	std::string::size_type f = unparsed.find('/');
	if (f != std::string::npos) {
		std::string registry = unparsed.substr(0, f);
		if ((registry.find('.') != std::string::npos) ||
			(registry.find(':') != std::string::npos) ||
			(registry == "localhost")) {

			_registry = registry;
			unparsed = unparsed.substr(f + 1);
		}
	}

	std::string::size_type g = unparsed.rfind("@");
	if (g == std::string::npos) {
		g = unparsed.rfind(":");
	}
	if (g != std::string::npos) {
		_prefix = unparsed[g];
		_reference = unparsed.substr(g + 1);
		unparsed = unparsed.substr(0, g);
	} else {
		_prefix = 0;
	}
	_repository = unparsed;
}

artifact_name::artifact_name(const parser& parsed):
	_registry(parsed.registry()),
	_repository(parsed.repository()),
	_reference(parsed.reference()) {}

artifact_name::artifact_name(const std::string& unparsed):
	artifact_name(parser(unparsed)) {}

artifact_name::operator std::string() const {
	std::string result;
	if (_registry) {
		result.append(*_registry);
		result.push_back('/');
	}
	result.append(_repository);
	if (_reference.is<repository_tag>()) {
		result.push_back(':');
	} else if (_reference.is<digest>()) {
		result.push_back('@');
	}
	result.append(_reference);
	return result;
}

artifact_name artifact_name::canonicalise() const {
	auto registry = _registry;
	auto repository = _repository;
	auto reference = _reference;

	if (!registry) {
		registry = default_registry_name;
	}
	if (*registry == default_registry_name) {
		repository = repository.canonicalise();
	}
	if (reference.empty()) {
		reference = default_repository_tag;
	}

	return artifact_name(registry, repository, reference);
}

artifact_name artifact_name::familiarise() const {
	auto registry = _registry;
	auto repository = _repository;
	auto reference = _reference;

	if (registry && (*registry == default_registry_name)) {
		registry = std::optional<registry_name>();
	}
	if (!registry) {
		repository = repository.familiarise();
	}
	if (reference.is<repository_tag>() &&
		reference.as<repository_tag>() == default_repository_tag) {

		reference = repository_reference();
	}

	return artifact_name(registry, repository, reference);
}

}
