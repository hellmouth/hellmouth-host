// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ARTIFACTS_ARTIFACT_NAME
#define HELLMOUTH_ARTIFACTS_ARTIFACT_NAME

#include <optional>
#include <string_view>

#include "hellmouth/artifacts/registry_name.h"
#include "hellmouth/artifacts/repository_name.h"
#include "hellmouth/artifacts/repository_tag.h"
#include "hellmouth/artifacts/digest.h"
#include "hellmouth/artifacts/repository_reference.h"

namespace hellmouth::artifacts {

/** A class to represent the name of an artifact.
 * The syntax of an artifact name is as follows:
 *
 * artifact-name ::= (registry-name "/")? repository-name
 *     (":" tag | "@" digest)?
 *
 * If no tag or digest is specified then a tag of "latest" is implied.
 *
 * Artifact names can be normalised to two different forms:
 * - canonical form, in which the registry and repository names are
 *   canonicalised, and either a tag or a digest is included; or
 * - familiar form, in which the registry and repository names are
 *   familiarised, and the tag is omitted if it can be implied,
 * but neither of these normalisations is performed by default.
 */
class artifact_name {
private:
	/** An optional registry name. */
	std::optional<registry_name> _registry;

	/** The name of the repository in which this artifact is located. */
	repository_name _repository;

	/** An optional reference to a specific version of the artifact.
	 * If this is empty then the tag "latest" is implied.
	 */
	repository_reference _reference;

	/** A class for parsing artifact names. */
	class parser;

	/** Construct artifact name using parser.
	 * @param parsed the parsed artifact name
	 */
	explicit artifact_name(const parser& parsed);
public:
	/** Construct artifact name from registry name, repository name and
	 * repository reference.
	 * @param registry an optional registry name
	 * @param repository the repository name
	 * @param reference an optional repository reference
	 */
	artifact_name(const std::optional<registry_name>& registry,
		const repository_name& repository,
		const repository_reference& reference):
		_registry(registry),
		_repository(repository),
		_reference(reference) {}

	/** Parse artifact name from character string.
	 * @param unparsed the unparsed artifact name
	 */
	explicit artifact_name(const std::string& unparsed);

	/** Convert this artifact name to a string.
	 * @return the artifact name as a string
	 */
	operator std::string() const;

	/** Get the registry name.
	 * @return the optional registry name
	 */
	const std::optional<registry_name>& registry() const {
		return _registry;
	}

	/** Get the repository name.
	 * @return the repository name
	 */
	const repository_name& repository() const {
		return _repository;
	}

	/** Get the artifact reference.
	 * @return the reference
	 */
	const repository_reference& reference() const {
		return _reference;
	}

	/** Canonicalise this repository name.
	 * @return this repository name, in canonical form
	 */
	artifact_name canonicalise() const;

	/** Familiarise this repository name.
	 * @return this repository name, in familiar form
	 */
	artifact_name familiarise() const;

	bool operator==(const artifact_name&) const = default;
	auto operator<=>(const artifact_name&) const = default;
};

inline std::string to_string(const artifact_name afname) {
	return afname;
}

}

#endif
