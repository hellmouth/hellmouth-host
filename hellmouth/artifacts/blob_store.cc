// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include <sys/stat.h>

#include "hellmouth/os/lockfile.h"
#include "hellmouth/os/utility.h"
#include "hellmouth/artifacts/algorithm.h"
#include "hellmouth/artifacts/registry.h"
#include "hellmouth/artifacts/blob_store.h"

namespace hellmouth::artifacts {

blob_store::blob_store(std::filesystem::path pathname):
	_pathname(std::move(pathname)) {

	create_directories(_pathname);
}

blob_store::ref blob_store::operator[](const digest& dgst) {
	std::string algname = dgst.algorithm();
	std::string value = dgst.value();
	auto pathname = _pathname / algname / value;
	return blob_store::ref(dgst, pathname);
}

class blob_store::verification_error:
	public std::runtime_error {
public:
	verification_error():
		runtime_error("blob content does not match digest") {}
};

bool blob_store::ref::verify(const std::filesystem::path& pathname) const {
	try {
		auto algo = make_algorithm(_dgst.algorithm());
		algo->read(pathname);
		auto calc_dgst = (*algo)();
		return calc_dgst == _dgst;
	} catch (std::system_error&) {
		return false;
	}
}

bool blob_store::ref::verify() const {
	return verify(_pathname);
}

void blob_store::ref::pull(const artifact_name& afname, bool secure) {
	// Make any required directories.
	create_directories(_pathname.parent_path());

	// Acquire the lockfile. This ensures that if a pull is already
	// in progress, the result will be detected.
	os::lockfile(make_lockname(_pathname));

	if (verify()) {
		// A verified local copy of the blob already exists, therefore there
		// is no need to fetch another copy from the registry.
		return;
	}

	// Pull the blob into a temporary file. Ownership of the lock file
	// prevents concurrent requests from interfering with the temporary file.
	std::filesystem::path tempname = make_tempname(_pathname);
	registry reg(*afname.registry(), secure);
	reg.pull_blob(afname, _dgst, tempname);

	// Verify the temporary file.
	if (!verify(tempname)) {
		// The temporary file could be deleted here, but there is
		// value in retaining the evidence following a verification
		// failure.
		throw verification_error();
	}

	rename(tempname, _pathname);
}

}
