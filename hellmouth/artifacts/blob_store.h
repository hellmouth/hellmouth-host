// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ARTIFACTS_BLOB_STORE
#define HELLMOUTH_ARTIFACTS_BLOB_STORE

#include <filesystem>

#include "hellmouth/artifacts/digest.h"
#include "hellmouth/artifacts/artifact_name.h"

namespace hellmouth::artifacts {

class digest;
class artifact_name;

/** A class for managing local blob storage.
 * Lockfiles are used to ensure that all operations are thread-safe,
 * provided that the blob store is located on a filesystem with standard
 * POSIX semantics.
 *
 * Current behaviour is to block until the lock in question can be acquired,
 * however this may change in the future.
 */
class blob_store {
private:
	/** The pathname at which the blob store is located. */
	std::filesystem::path _pathname;
public:
	/** Report that a blob failed verification against its digest. */
	class verification_error;

	/** A class for referring to a blob with a given digest.
	 * It is not necessary for the blob to be present in the local store.
	 */
	class ref;

	/** Construct local blob store.
	 * @param pathname the pathname at which the blob store is located
	 */
	blob_store(std::filesystem::path pathname);

	/** Get an object to represent a blob with a given digest.
	 * It is not necessary for the blob to exist within the local store.
	 */
	ref operator[](const digest& dgst);
};

class blob_store::ref {
private:
	/** The digest of this blob. */
	digest _dgst;

	/** The pathname at which this blob is or would be located. */
	std::filesystem::path _pathname;
public:
	/** Construct blob reference.
	 * @param dgst the digest of this blob.
	 * @param pathname the pathname at which this blob is or would be located.
	 */
	ref(const digest& dgst, std::filesystem::path pathname):
		_dgst(dgst), _pathname(std::move(pathname)) {}

	/** Get the pathname at which this blob is or would be located
	 * @return the pathname
	 */
	const std::filesystem::path& pathname() const {
		return _pathname;
	}

	/** Verify that the file at a given pathname matches this blob digest.
	 * @param pathname the pathname of the file to be verified
	 * @return true if successfully verified, otherwise false
	 */
	bool verify(const std::filesystem::path& pathname) const;

	/** Verify that the content of this blob matches its digest.
	 * @return true if successfully verified, otherwise false
	 */
	bool verify() const;

	/** Pull this blob from a given repository.
	 * @param afname an artifact name which identifies the repository
	 * @param secure true if using a secure registry, false if insecure
	 */
	void pull(const artifact_name& afname, bool secure);
};

}

#endif
