// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include "hellmouth/artifacts/digest.h"

namespace hellmouth::artifacts {

class digest::parser {
private:
	std::string _algorithm;
	std::string _value;
public:
	parser(std::string_view unparsed);
	const std::string& algorithm() const {
		return _algorithm;
	}
	const std::string& value() const {
		return _value;
	}
};

digest::parser::parser(std::string_view unparsed) {
	std::string::size_type f = unparsed.find(':');
	if (f == std::string::npos) {
		throw std::invalid_argument("invalid digest");
	}
	_algorithm = unparsed.substr(0, f);
	_value = unparsed.substr(f + 1);
}

digest::digest(const parser& parsed):
	_algorithm(parsed.algorithm()),
	_value(parsed.value()) {}

digest::digest(std::string_view unparsed):
	digest(parser(unparsed)) {}

digest::operator std::string() const {
	return static_cast<std::string>(_algorithm) + ':' +
		static_cast<std::string>(_value);
}

}
