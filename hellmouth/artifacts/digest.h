// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ARTIFACT_DIGEST
#define HELLMOUTH_ARTIFACT_DIGEST

#include <string>

#include "hellmouth/artifacts/algorithm_name.h"
#include "hellmouth/artifacts/encoded_digest_value.h"

namespace hellmouth::artifacts {

/** A class to represent a validated digest.
 * A valid digest matches the following syntax:
 *
 * digest ::= algorithm-name ":" encoded-digest-value
 */
class digest {
private:
	/** The name of the algorithm used to calculate this digest. */
	algorithm_name _algorithm;

	/** The value of this digest. */
	encoded_digest_value _value;

	/** A class for parsing digests. */
	class parser;

	/** Construct digest from parser instance.
	 * @param parsed the parsed digest
	 */
	explicit digest(const parser& parsed);
public:
	/** Construct digest from algorithm name and digest value.
	 * @param algorithm the algorithm name
	 * @param value the encoded digest value
	 */
	digest(const algorithm_name& algorithm,
		const encoded_digest_value& value):
		_algorithm(algorithm),
		_value(value) {}

	/** Parse digest from character string.
	 * @param value the digest as a std::string_view
	 */
	explicit digest(std::string_view value);

	/** Get this algorithm name as a string.
	 * @return the algorithm name
	 */
	operator std::string() const;

	/** Get the algorithm name.
	 * @return the algorithm name
	 */
	const algorithm_name& algorithm() const {
		return _algorithm;
	}

	/** Get the encoded digest value.
	 * @return the encoded digest value
	 */
	const encoded_digest_value& value() const {
		return _value;
	}

	bool operator==(const digest&) const = default;
	auto operator<=>(const digest&) const = default;
};

inline std::string to_string(const digest& dgst) {
	return dgst;
}

}

#endif
