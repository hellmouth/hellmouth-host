// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>
#include <regex>

#include "hellmouth/artifacts/encoded_digest_value.h"

namespace hellmouth::artifacts {

encoded_digest_value::encoded_digest_value(std::string value):
	_value(std::move(value)) {

	static const std::regex digest_pattern(R"([a-zA-Z0-9=_-]+)");
	if (!std::regex_match(_value, digest_pattern)) {
		throw std::invalid_argument("invalid digest");
	}
}

}
