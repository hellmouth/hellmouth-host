// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ARTIFACTS_ENCODED_DIGEST_VALUE
#define HELLMOUTH_ARTIFACTS_ENCODED_DIGEST_VALUE

#include <string>

namespace hellmouth::artifacts {

/** A class to represent the encoded value of a digest.
 * An encoded digest value is valid if it matches the following production:
 *
 * encoded-digest-value ::= [a-zA-Z0-9=_-]+
 *
 * (Following the OCI image spec v1.1.0, descriptor.md#digests.)
 */
class encoded_digest_value {
private:
	/** The digest value as a string. */
	std::string _value;
public:
	/** Construct digest value from string.
	 * @param value the digest value as a string
	 */
	explicit encoded_digest_value(std::string value);

	/** Get this digest value as a string.
	 * @return the digest value
	 */
	operator const std::string&() const {
		return _value;
	}

	bool operator==(const encoded_digest_value&) const = default;
	auto operator<=>(const encoded_digest_value&) const = default;
};

inline const std::string& to_string(const encoded_digest_value& encdval) {
	return encdval;
}

}

#endif
