// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cctype>
#include <cstdint>
#include <stdexcept>

#include "hellmouth/artifacts/hex_digest_value.h"

namespace hellmouth::artifacts {

class hex_digest_value::encoder {
private:
	std::string _result;
public:
	encoder(const void* data, size_t length);

	operator const std::string&() const {
		return _result;
	}
};

hex_digest_value::encoder::encoder(const void* data, size_t length) {
	_result.reserve(2 * length);
	const uint8_t* octets = static_cast<const uint8_t*>(data);
	const char* hex = "0123456789abcdef";
	for (size_t i = 0; i != length; ++i) {
		unsigned char octet = octets[i];
		_result.push_back(hex[octet >> 4]);
		_result.push_back(hex[octet & 0xf]);
	}
}

hex_digest_value::hex_digest_value(const encoder& encoded):
	encoded_digest_value(encoded) {}

hex_digest_value::hex_digest_value(const void* data, size_t length):
	hex_digest_value(encoder(data, length)) {}

}
