// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ARTIFACTS_HEX_DIGEST_VALUE
#define HELLMOUTH_ARTIFACTS_HEX_DIGEST_VALUE

#include <cstddef>

#include "hellmouth/artifacts/encoded_digest_value.h"

namespace hellmouth::artifacts {

/** A class to represent the hexadecimal encoded value of a digest. */
class hex_digest_value:
	public encoded_digest_value {
private:
	/** A class for encoding hex values. */
	class encoder;

	/** Construct hex digest from encoder.
	 * @param encoded the encoded value
	 */
	explicit hex_digest_value(const encoder& encoded);
public:
	/** Construct digest value from raw data.
	 * @param data the data to be encoded
	 * @param length the length of the data to be encoded
	 */
	hex_digest_value(const void* data, size_t length);
};

}

#endif
