// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>
#include <cstdio>
#include <fstream>

#include <sys/stat.h>

#include "hellmouth/data/any.h"
#include "hellmouth/json/decoder.h"
#include "hellmouth/artifacts/digest.h"
#include "hellmouth/artifacts/artifact_name.h"
#include "hellmouth/artifacts/registry.h"
#include "hellmouth/artifacts/image_store.h"

namespace hellmouth::artifacts {

class image_store::unrecognised_image_format_error:
	public std::runtime_error {
public:
	unrecognised_image_format_error(const std::string& msg):
		std::runtime_error(msg) {}
};

image_store::image_store(std::filesystem::path pathname):
	_pathname(std::move(pathname)),
	_blobs(_pathname / "blobs"),
	_manifests(_pathname / "manifests"),
	_layers(_pathname / "layers") {}

image_store::ref image_store::operator[](const artifact_name& afname) {
	return ref(afname, *this);
}

void image_store::ref::pull(bool secure) {
	// Pull and decode manifest.
	auto mf_ref = _images->_manifests[_afname];
	mf_ref.pull(secure);
	std::ifstream mf_stream(mf_ref.pathname());
	json::decoder mf_decoder(mf_stream);
	data::any mf = mf_decoder();
	std::string mf_type = mf.at("mediaType");
	if (mf_type != "application/vnd.docker.distribution.manifest.v2+json") {
		throw unrecognised_image_format_error(
			"unrecognised media type for image manifest");
	}

	// Pull config blob.
	data::any config = mf.at("config");
	std::string config_type = config.at("mediaType");
	if (config_type != "application/vnd.docker.container.image.v1+json") {
		throw unrecognised_image_format_error(
			"unrecognised media type for image config");
	}
	digest config_dgst(config.at("digest"));
	auto config_blob = _images->_blobs[config_dgst];
	config_blob.pull(_afname, secure);

	// Pull and unpack layer blobs.
	for (auto& layer : to_array(mf.at("layers"))) {
		std::string layer_type = layer.at("mediaType");
		if (layer_type !=
			"application/vnd.docker.image.rootfs.diff.tar.gzip") {
			throw unrecognised_image_format_error(
				"unrecognised media type for image layer");
		}
		digest layer_dgst(layer.at("digest"));
		auto layer_blob = _images->_blobs[layer_dgst];
		layer_blob.pull(_afname, secure);
		_images->_layers[layer_dgst].unpack(layer_blob.pathname());
	}
}

std::vector<std::filesystem::path> image_store::ref::layers() const {
	std::ifstream mf_stream(_images->_manifests[_afname].pathname());
	data::any mf = json::decoder(mf_stream)();

	std::vector<std::filesystem::path> result;
	for (auto& layer: to_array(mf["layers"])) {
		digest layer_dgst(layer["digest"]);
		result.push_back(_images->_layers[layer_dgst].pathname());
	}
	return result;
}

}
