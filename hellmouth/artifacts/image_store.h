// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ARTIFACTS_IMAGE_STORE
#define HELLMOUTH_ARTIFACTS_IMAGE_STORE

#include <vector>
#include <string>
#include <filesystem>

#include "hellmouth/artifacts/blob_store.h"
#include "hellmouth/artifacts/manifest_store.h"
#include "hellmouth/artifacts/layer_store.h"

namespace hellmouth::artifacts {

/** A class for managing local image storage. */
class image_store {
private:
	/** The pathname at which the image store as a whole is located. */
	std::filesystem::path _pathname;

	/** The local blob store. */
	blob_store _blobs;

	/** The local manifest store. */
	manifest_store _manifests;

	/** The local layer store. */
	layer_store _layers;
public:
	/** Report that the image was not in a recognised format. */
	class unrecognised_image_format_error;

	/** A class for referring to the image with a given artifact name.
	 * It is not necessary for the image to be present in the local store.
	 */
	class ref;
	friend class ref;

	/** A class for iterating over the available images. */
	typedef manifest_store::iterator iterator;

	/** Construct local image store.
	 * @param pathname the pathname at which the store as a whole is located
	 */
	image_store(std::filesystem::path pathname);

	/** Get an object to represent the image with a given artifact name.
	 * It is not necessary for the image to exist within the local store.
	 */
	ref operator[](const artifact_name& afname);

	/** Get an iterator for the start of the sequence of images.
	 * @return the iterator
	 */
	iterator begin() const {
		return _manifests.begin();
	}

	/** Get an iterator for the end of the sequence of images.
	 * @return the iterator
	 */
	iterator end() const {
		return _manifests.end();
	}
};

class image_store::ref {
private:
	/** The artifact name for this image. */
	artifact_name _afname;

	/** The containing image store. */
	image_store* _images;
public:
	/** Construct image reference.
	 * @param afname the artifact name for this image
	 * @param images the containing image store
	 */
	ref(const artifact_name& afname, image_store& images):
		_afname(afname), _images(&images) {}

	/** Pull this image from the relevant repository.
	 * This will cause the following steps to be performed:
	 *
	 * - The manifest is pulled from the repository.
	 * - The config blob is pulled from the repository.
	 * - Each layer blob is pulled from the repository then unpacked.
	 *
	 * Some steps may be omitted if they have already been performed
	 * previously (normal rules apply for this), but doing that does
	 * not affect subsequent steps.
	 *
	 * The calling process must be able to freely set file ownership and
	 * permissions.
	 *
	 * @param secure true if using a secure registry, false if insecure
	 */
	void pull(bool secure);

	/** Get a list of pathnames for the layers of this image.
	 * @return the list of layers
	 */
	std::vector<std::filesystem::path> layers() const;
};

}

#endif
