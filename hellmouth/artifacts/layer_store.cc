// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <unistd.h>

#include "hellmouth/os/lockfile.h"
#include "hellmouth/os/command.h"
#include "hellmouth/os/utility.h"
#include "hellmouth/artifacts/layer_store.h"

namespace hellmouth::artifacts {

layer_store::layer_store(std::filesystem::path pathname):
	_pathname(std::move(pathname)) {

	// [TODO]: investigate whether there is an exploitable race
	// condition here. There might not be due to Linux supporting
	// O_PATH in place of O_SEARCH, but have not found any strong
	// evidence that the current methods are safe.
	//
	// Options using std::filesystem are limited, and it may be
	// necessary to revert to mkdir if a better method is needed.
	create_directories(_pathname);
	permissions(_pathname, std::filesystem::perms::owner_all);
}

layer_store::ref layer_store::operator[](const digest& dgst) {
	std::filesystem::path dirname = _pathname / to_string(dgst.algorithm());
	create_directories(dirname);
	return ref(dgst, dirname / to_string(dgst.value()));
}

void layer_store::ref::unpack(const std::filesystem::path& blob_pathname) {
	// Acquire the lockfile first. This ensures that if an unpack is already
	// in progress, the result will be detected.
	os::lockfile(make_lockname(_pathname));

	// If an unpacked copy of this layer already exists, return immediately.
	if (exists(_pathname)) {
		return;
	}

	// Unpack the layer into a temporary directory.
	auto tempname = make_tempname(_pathname);
	create_directory(tempname);
	int rc = os::command("/usr/bin/tar")
		.append("-z")
		.append("-x")
		.append("-f").append(blob_pathname)
		.append("--same-owner")
		.append("--same-permissions")
		.chdir(tempname)
		.exec();

	// Move the unpacked layer to its permanent location.
	if (rc == 0) {
		rename(tempname, _pathname);
	}
}

}
