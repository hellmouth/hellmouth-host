// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ARTIFACTS_LAYER_STORE
#define HELLMOUTH_ARTIFACTS_LATER_STORE

#include <filesystem>

#include "hellmouth/artifacts/digest.h"

namespace hellmouth::artifacts {

class digest;
class artifact_name;

/** A class for storing filesystem layers.
 * Lockfiles are used to ensure that all operations are thread-safe.
 * Current behaviour is to block until the lock in question can be
 * acquired, however this may change in the future.
 *
 * Read access to the layer store is restricted due to the possibility
 * that it might contain unsafe SUID or SGID executables.
 */
class layer_store {
private:
	/** The pathname at which the layer store is located. */
	std::filesystem::path _pathname;
public:
	/** A class for referring to a layer with a given digest.
	 * It is not necessary for the layer to be present in the local store.
	 */
	class ref;

	/** Construct layer store.
	 * @param pathname the pathname at which the layer store is located
	 */
	layer_store(std::filesystem::path pathname);

	/** Get an object to represent a layer with a given digest.
	 * It is not necessary for the layer to exist within the local store.
	 */
	ref operator[](const digest& dgst);
};

class layer_store::ref {
private:
	/** The digest of this layer. */
	digest _dgst;

	/** The pathname at which this layer is or would be located. */
	std::filesystem::path _pathname;
public:
	/** Construct layer reference.
	 * @param dgst the digest of this layer.
	 * @param pathname the pathname at which this layer is or would be located.
	 * @param client an HTTP client for use when interacting with registries
	 */
	ref(digest dgst, std::filesystem::path pathname):
		_dgst(std::move(dgst)), _pathname(std::move(pathname)) {}

	/** Get the pathname at which this layer is or would be located
	 * @return the pathname
	 */
	const std::filesystem::path& pathname() const {
		return _pathname;
	}

	/** Unpack this layer from a given pathname.
	 * The calling process must be able to freely set file ownership and
	 * permissions.
	 * @param pathname the pathname of the blob containing the packed layer
	 */
	void unpack(const std::filesystem::path& blob_pathname);
};

}

#endif
