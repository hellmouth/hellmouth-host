// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <string>

#include "hellmouth/os/lockfile.h"
#include "hellmouth/os/utility.h"
#include "hellmouth/artifacts/artifact_name.h"
#include "hellmouth/artifacts/registry.h"
#include "hellmouth/artifacts/manifest_store.h"

namespace hellmouth::artifacts {

manifest_store::manifest_store(std::filesystem::path pathname):
	_pathname(std::move(pathname)) {

	create_directories(_pathname);
}

manifest_store::ref manifest_store::operator[](const artifact_name& afname) {
	auto pathname = _pathname
		/ to_string(*afname.registry())
		/ to_string(afname.repository())
		/ (std::string("_") + to_plural(afname.reference()))
		/ to_string(afname.reference());
	create_directories(pathname.parent_path());
	return ref(afname, pathname);
}

void manifest_store::ref::pull(bool secure) {
	// Acquire the lockfile.
	os::lockfile(make_lockname(_pathname));

	// Pull the manifest into a temporary file. Ownership of the lock file
	// prevents concurrent requests from interfering with the temporary file.
	auto tempname = make_tempname(_pathname);
	registry reg(*_afname.registry(), secure);
	reg.pull_manifest(_afname, tempname);

	// Move the file to its permanent location.
	rename(tempname, _pathname);
}

manifest_store::iterator manifest_store::begin() const {
	return iterator(_pathname);
}

manifest_store::iterator manifest_store::end() const {
	return iterator();
}

bool manifest_store::iterator::_decode() {
	if (!_iter->is_regular_file()) {
		return false;
	}
	std::string namestr = std::filesystem::relative(
		_iter->path(), _pathname);

	auto f = namestr.rfind("/_");
	if (f == namestr.npos) {
		return false;
	}
	if (namestr.substr(f, 7) == "/_tags/") {
		namestr.replace(f, 7, ":");
	} else if (namestr.substr(f, 10) == "/_digests/") {
		namestr.replace(f, 7, "@");
	} else {
		return false;
	}
	_name = artifact_name(namestr);
	return true;
}

void manifest_store::iterator::_ensure() {
	static std::filesystem::recursive_directory_iterator end;
	while (_iter != end && !_decode()) {
		++_iter;
	}
}

manifest_store::iterator::iterator(
	const std::filesystem::path pathname):
	_pathname(std::move(pathname)),
	_iter(_pathname) {

	_ensure();
}

manifest_store::iterator& manifest_store::iterator::operator++() {
	++_iter;
	_ensure();
	return *this;
}

}
