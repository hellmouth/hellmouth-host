// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ARTIFACTS_MANIFEST_STORE
#define HELLMOUTH_ARTIFACTS_MANIFEST_STORE

#include <filesystem>

#include "hellmouth/artifacts/artifact_name.h"

namespace hellmouth::artifacts {

class artifact_name;

/** A class to represent local manifest storage.
 * Lockfiles are used to ensure that all operations are thread-safe,
 * provided that the blob store is located on a filesystem with standard
 * POSIX semantics.
 *
 * Current behaviour is to block until the lock in question can be acquired,
 * however this may change in the future.
 */
class manifest_store {
private:
	/** The pathname at which the manifest store is located. */
	std::filesystem::path _pathname;
public:
	/** A class for referring to the manifest for a given artifact name.
	 * It is not necessary for the manifest to be present in the local store.
	 */
	class ref;

	/** A class for iterating over manifests in this store. */
	class iterator;

	/** Construct local manifest store.
	 * @param pathname the pathname at which the manifest store is located
	 */
	manifest_store(std::filesystem::path pathname);

	/** Get an object to represent the manifest for a given artifact name.
	 * It is not necessary for the manifest to exist within the local store.
	 */
	ref operator[](const artifact_name& afname);

	/** Get an iterator for the start of the sequence of manifests.
	 * @return the iterator
	 */
	iterator begin() const;

	/** Get an iterator for the end of the sequence of manifests.
	 * @return the iterator
	 */
	iterator end() const;
};

class manifest_store::ref {
private:
	/** The artifact name for this manifest. */
	artifact_name _afname;

	/** The pathname at which this manifest is or would be located. */
	std::filesystem::path _pathname;
public:
	/** Construct manifest reference.
	 * @param afname the artifact name for this manifest
	 * @param pathname the pathname at which this manifest is or would be
	 *  located.
	 */
	ref(const artifact_name& afname, std::filesystem::path pathname):
		_afname(std::move(afname)),
		_pathname(std::move(pathname)) {}

	/** Get the pathname at which this manifest is or would be located
	 * @return the pathname
	 */
	const std::filesystem::path& pathname() const {
		return _pathname;
	}

	/** Pull this manifest from the relevant repository.
	 * Current practice is to perform a full download of the manifest
	 * regardless of what is available locally. This may be optimised to
	 * avoid unnecessary data transfer in the future, however the registry
	 * must always be contacted when referenced by tag, because in that
	 * case the manifest could have changed since it was previously pulled.
	 *
	 * @param secure true if using a secure registry, false if insecure
	 */
	void pull(bool secure);
};

class manifest_store::iterator {
private:
	/** The base pathpath of the manifest store. */
	std::filesystem::path _pathname;

	/** An iterator for searching for manifest files.
	 * Note that in addition to the manifest files themselves, this will
	 * also return the containing directories, and any temporary files or
	 * lock files. Other extraneous content is possible. */
	std::filesystem::recursive_directory_iterator _iter;

	/** The cached artefact name.
	 * This is the value which the iterator yields if dereferenced.
	 * It should always contain a usable value, excepting when the
	 * iterator has reached the end of the sequence in which case its
	 * value is unspecified. */
	std::optional<artifact_name> _name;

	/** Attempt to decode current pathname.
	 * If successful then the result is cached in _name. That will only
	 * be the case if:
	 *
	 * - the filesystem object referred to by _iter is a regular file,
	 *   and
	 * - its pathname can be converted into a valid artifact name.
	 *
	 * @return true if successful, otherwise false.
	 */
	bool _decode();

	/** Attempt to ensure that this iterator refers to a manifest file.
	 * If _iter does not refer to a manifest file then it will be
	 * advanced until either it does refer to a manifest file, or the
	 * end of the sequence has been reached. In the former case, the
	 * name of the artifact will be cached in _name.
	 */
	void _ensure();
public:
	/** Construct iterator corresponding to the end of the sequence. */
	iterator() = default;

	/** Construct iterator corresponding to the start of the sequence.
	 * @param pathname the base pathname of the manifest store
	 */
	iterator(std::filesystem::path pathname);

	/** Dereference this iterator.
	 * @return the name of the artifact referred to
	 */
	const artifact_name& operator*() const {
		return *_name;
	}

	/** Move this iterator forward by one step.
	 * @return a reference to this
	 */
	iterator& operator++();

	/** Compare this iterator with another.
	 * The value of _pathname is disregarded so that the end of sequence
	 * does not need to know the pathname or be treated as a special case.
	 * The value of _name is disregarded because it acts only as a cache.
	 *
	 * @return true if equal, otherwise false
	 */
	bool operator==(const iterator& that) const {
		return _iter == that._iter;
	}
};

}

#endif
