// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <fstream>

#include "hellmouth/net/http/error.h"
#include "hellmouth/net/http/request.h"
#include "hellmouth/artifacts/digest.h"
#include "hellmouth/artifacts/artifact_name.h"
#include "hellmouth/artifacts/registry.h"

namespace hellmouth::artifacts {

registry::registry(const registry_name& regname, bool secure) {
	if (secure) {
		_base_url = "https://";
	} else {
		_base_url = "http://";
	}
	_base_url += to_string(regname);
}

void registry::pull_manifest(const artifact_name& afname,
	std::filesystem::path pathname) {

	std::string url = _base_url + "/v2/" +
		to_string(afname.repository()) +
		"/manifests/" +
		to_string(afname.reference());
	net::http::request req("GET", url);
	req.headers.push_back("Accept",
		"application/vnd.docker.distribution.manifest.v2+json, "
		"application/vnd.docker.distribution.manifest.list.v2+json");
	net::http::response resp = fetch(req);
	if (resp.status_code != 200) {
		throw net::http::error(resp.status_code);
	}

	std::ofstream out(pathname);
	out << resp.body.content().rdbuf();
}

void registry::pull_blob(const artifact_name& afname, const digest& dgst,
	std::filesystem::path pathname) {

	std::string url = _base_url + "/v2/" +
		to_string(afname.repository()) +
		"/blobs/" +
		to_string(dgst);
	net::http::request req("GET", url);
	net::http::response resp = fetch(req);
	if (resp.status_code != 200) {
		throw net::http::error(resp.status_code);
	}

	std::ofstream out(pathname);
	out << resp.body.content().rdbuf();
}

}
