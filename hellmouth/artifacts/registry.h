// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ARTIFACTS_REGISTRY
#define HELLMOUTH_ARTIFACTS_REGISTRY

#include <string>
#include <filesystem>

#include "hellmouth/artifacts/registry_name.h"

namespace hellmouth::artifacts {

class digest;
class artifact_name;

/** A class to represent an artifact registry. */
class registry {
private:
	/** The base URL for accessing the registry.
	 * This includes the scheme and authority components, with no
	 * trailing slash. The API version is not included.
	 */
	std::string _base_url;
public:
	/** Construct artifact registry.
	 * @param regname the name of the registry to be accessed
	 * @param secure true if a secure registry, false if insecure
	 */
	registry(const registry_name& regname, bool secure);

	/** Pull manifest from registry to specified local pathname.
	 * [TODO]: there is currently little error handling or validation.
	 *
	 * @param afname the artifact name, which must be in canonical form
	 * @param pathname the pathname to write the manifest
	 */
	void pull_manifest(const artifact_name& afname,
		std::filesystem::path pathname);

	/** Pull blob from registry to specified local pathname.
	 * [TODO]: there is currently little error handling or validation.
	 *
	 * @param afname the artifact name, which must be in canonical form
	 * @param pathname the pathname to write the blob content
	 */
	void pull_blob(const artifact_name& afname, const digest& dgst,
		std::filesystem::path pathname);
};

}

#endif
