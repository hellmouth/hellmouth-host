// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cctype>
#include <stdexcept>

#include "hellmouth/artifacts/registry_name.h"

namespace hellmouth::artifacts {

class registry_name::parser {
private:
	std::string _host;
	std::optional<std::string> _port;
public:
	explicit parser(std::string_view unparsed);
	net::hostname host() const {
		return net::hostname(_host);
	}
	std::optional<net::port_number> port() const {
		std::optional<net::port_number> result;
		if (_port) {
			result = net::port_number(*_port);
		}
		return result;
	}
};

registry_name::parser::parser(std::string_view unparsed) {
	std::string::size_type f = unparsed.find(':');
	if (f != std::string::npos) {
		_port = unparsed.substr(f + 1);
	}
	_host = unparsed.substr(0, f);
}

registry_name::registry_name(const parser& parsed):
	_host(parsed.host()), _port(parsed.port()) {}

registry_name::registry_name(std::string_view unparsed):
	registry_name(parser(unparsed)) {}

registry_name::operator std::string() const {
	std::string result = static_cast<std::string>(_host);
	if (_port) {
		result.append(":");
		result.append(static_cast<std::string>(*_port));
	}
	return result;
}

registry_name default_registry_name("docker.io");

}
