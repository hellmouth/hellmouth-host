// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ARTIFACTS_REGISTRY_NAME
#define HELLMOUTH_ARTIFACTS_REGISTRY_NAME

#include <optional>
#include <string>
#include <string_view>

#include "hellmouth/net/hostname.h"
#include "hellmouth/net/port_number.h"

namespace hellmouth::artifacts {

/** A class to represent a validated registry name.
 * A registry name is valid if it matches the following syntax:
 *
 * registry-name ::= hostname (":" port-number)?
 *
 * Additionally, the port number must be in the range 1-65535 inclusive.
 * Productions for hostname and port-number are given in the corresponding
 * classes.
 */
class registry_name {
private:
	/** The hostname. */
	net::hostname _host;

	/** The port number. */
	std::optional<net::port_number> _port;

	/** A class for parsing registry names. */
	class parser;

	/** Construct registry name using parser.
	 * @param parsed the parsed registry name
	 */
	explicit registry_name(const parser& parsed);
public:
	/** Parse registry name from character string.
	 * @param unparsed the unparsed registry name
	 */
	explicit registry_name(std::string_view unparsed);

	/** Convert this regsitry name to a string.
	 * @return the name as a string
	 */
	operator std::string() const;

	/** Get the hostname component.
	 * @return the hostname
	 */
	const net::hostname& host() const {
		return _host;
	}

	/** Get the optional port number component.
	 * @return an optional port number
	 */
	const std::optional<net::port_number>& port() const {
		return _port;
	}

	bool operator==(const registry_name&) const = default;
	auto operator<=>(const registry_name&) const = default;
};

inline std::string to_string(const registry_name& regname) {
	return regname;
}

/** The default registry name that is used when one has not been
 * explicitly specified in an artifact name. */
extern registry_name default_registry_name;

}

#endif
