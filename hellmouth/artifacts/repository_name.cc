// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>
#include <format>
#include <regex>

#include "hellmouth/artifacts/repository_name.h"

namespace hellmouth::artifacts {

static const std::string default_prefix("library/");

repository_name::repository_name(std::string_view name):
	_name(name) {

	static const std::regex repository_pattern(
		std::format("{0}(/{0})*",
		R"([a-z0-9]+((\.|_|__|-+)[a-z0-9]+)*)"));

	if (!std::regex_match(_name, repository_pattern)) {
		throw std::invalid_argument("invalid repository name");
	}
}

repository_name repository_name::canonicalise() const {
	if (_name.find('/') == std::string::npos) {
		return repository_name(default_prefix + _name);
	} else {
		return *this;
	}
}

repository_name repository_name::familiarise() const {
	if (_name.starts_with(default_prefix) &&
		_name.find('/', default_prefix.length()) == std::string::npos) {

		return repository_name(_name.substr(default_prefix.length()));
	} else {
		return *this;
	}
}

}
