// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ARTIFACTS_REPOSITORY_NAME
#define HELLMOUTH_ARTIFACTS_REPOSITORY_NAME

#include <string>
#include <string_view>

namespace hellmouth::artifacts {

/** A class to represent the validated repository name.
 * A repository name is valid if it matches the following syntax:
 *
 * repository-name ::= repository-name-segment ("/" repository-name-segment)*
 * repository-name-segment ::= [a-z0-9]+(\.|_|__|-+][a-z0-9]+)*
 *
 * (This is based on the syntax given in the OCI Distribution Specification.
 * Note that this differs from the syntax permitted by Docker.)
 */
class repository_name {
private:
	/** The repository name, as a string. */
	std::string _name;
public:
	/** Construct repository name from character string.
	 * @param name the repository name as a string_view
	 */
	explicit repository_name(std::string_view name);

	/** Convert this repository name to a string.
	 * @return the repository name as a string
	 */
	operator const std::string&() const {
		return _name;
	}

	/** Canonicalise this repository name.
	 * Canonicalisation is appropriate only if the name relates to the default
	 * registry (either named explicitly or implied by absence). This function
	 * assumes that to be the case, so should not be called unless that has
	 * been established.
	 *
	 * When canonicalised, names containing a single segment have the default
	 * prefix ("library/") added. Other names are unaffected.
	 *
	 * @return the canonicalised name
	 */
	repository_name canonicalise() const;

	/** Familiarise this repository name.
	 * Familiarisation is appropriate only if the name relates to the default
	 * registry (either named explicitly or implied by absence). This function
	 * assumes that to be the case, so should not be called unless that has
	 * been established.
	 *
	 * When familiarised, names beginning with the default prefix ("library/")
	 * have this removed if and only if there is exactly one other segment.
	 *
	 * @return the familiarised name
	 */
	repository_name familiarise() const;

	bool operator==(const repository_name&) const = default;
	auto operator<=>(const repository_name&) const = default;
};

inline const std::string& to_string(const repository_name& reponame) {
	return reponame;
}

}

#endif
