// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/artifacts/repository_reference.h"

namespace hellmouth::artifacts {

repository_reference::repository_reference(
	char prefix, std::string_view value) {

	switch (prefix) {
	case ':':
		_value = repository_tag(value);
		break;
	case '@':
		_value = digest(value);
		break;
	default:
		throw std::invalid_argument("invalid tag or digest");
	}
}

namespace {

struct string_visitor {
	std::string operator()(const std::monostate&) const {
		throw std::invalid_argument("empty repository reference");
	}

	std::string operator()(const repository_tag& value) const {
		return value;
	}

	std::string operator()(const digest& value) {
		return value;
	}
};

}

repository_reference::operator std::string() const {
	return std::visit(string_visitor(), _value);
}

namespace {

struct plural_visitor {
	const std::string& operator()(const std::monostate&) const {
		throw std::invalid_argument("empty repository reference");
	}

	const std::string& operator()(const repository_tag&) const {
		static std::string plural("tags");
		return plural;
	}

	const std::string& operator()(const digest&) const {
		static std::string plural("digests");
		return plural;
	}
};

}

const std::string& to_plural(const repository_reference& ref) {
	plural_visitor visitor;
	return ref.visit(visitor);
}

}
