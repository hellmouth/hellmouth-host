// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ARTIFACTS_REPOSITORY_REFERENCE
#define HELLMOUTH_ARTIFACTS_REPOSITORY_REFERENCE

#include <variant>

#include "hellmouth/artifacts/repository_tag.h"
#include "hellmouth/artifacts/digest.h"

namespace hellmouth::artifacts {

/** A class to represent a repository reference.
 * A reference selects a version of an artifact. It can be either:
 *
 * - a tag, which has a user-chosen value, and may refer to different verions
 *   of the artifact over time, or
 * - a digest, which has a computed value which always refers to the same
 *   version of the artifact.
 *
 * When a reference is used as part of an artifact name, it is preceded by a
 * prefix character (":" for tags, "@" for digests). Note that ":" can (and
 * indeed must) occur within a digest, so care is needed when parsing. Prefix
 * characters are not used in the registry REST API.
 */
class repository_reference {
private:
	/** The underlying value of this repository reference. */
	std::variant<std::monostate, repository_tag, digest> _value;
public:
	/** Construct empty repository reference. */
	repository_reference() = default;

	/** Parse repository reference from character string.
	 * @param prefix the prefix character
	 * @param value the remainder as a std::string_view
	 */
	repository_reference(char prefix, std::string_view refstr);

	/** Assign a repository tag.
	 * @param that the repository tag to be assigned
	 * @return a reference to this
	 */
	repository_reference& operator=(const repository_tag& that) {
		_value = that;
		return *this;
	}

	/** Assign a digest.
	 * @param that the digest to be assigned
	 * @return a reference to this
	 */
	repository_reference& operator=(const digest& that) {
		_value = that;
		return *this;
	}

	/** Convert this reference to a string.
	 * The result will not contain the prefix character
	 *
	 * @return the reference as a string
	 */
	operator std::string() const;

	/** Check whether this repository reference is empty.
	 * @return true if empty, otherwise false
	 */
	bool empty() const {
		return _value.index() == 0;
	}

	template<class T>
	bool is() const {
		return std::holds_alternative<T>(_value);
	}

	template<class T>
	const T& as() const {
		return std::get<T>(_value);
	}

	/** Visit the content with a given functor.
	 * The functor should be able to act on a const repository tag,
	 * a digest, or a std::monotype.
	 *
	 * @param visitor the functor to be applied
	 */
	template<class T>
	decltype(auto) visit(T& visitor) const {
		return std::visit(visitor, _value);
	}

	bool operator==(const repository_reference&) const = default;
	auto operator<=>(const repository_reference&) const = default;
};

inline std::string to_string(const repository_reference& ref) {
	return ref;
}

/** Get the plural noun used to describe a given reference type.
 * This is either "tags" or "digests" as appropriate.
 *
 * @return the plural noun
 */
const std::string& to_plural(const repository_reference& ref);

}

#endif
