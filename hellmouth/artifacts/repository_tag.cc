// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>
#include <regex>

#include "hellmouth/artifacts/repository_tag.h"

namespace hellmouth::artifacts {

repository_tag::repository_tag(std::string_view value):
	_value(value) {

	static const std::regex tag_pattern(
		R"([a-zA-Z0-9_][a-zA-Z0-9._-]{0,127})");
	if (!std::regex_match(_value, tag_pattern)) {
		throw std::invalid_argument("invalid tag");
	}
}

repository_tag default_repository_tag("latest");

}
