// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ARTIFACTS_REPOSITORY_TAG
#define HELLMOUTH_ARTIFACTS_REPOSITORY_TAG

#include <string>
#include <string_view>

namespace hellmouth::artifacts {

/** A class to represent a validated repository tag.
 * A tag is valid if and only if it matches the following syntax:
 *
 * tag ::= [a-zA-Z0-9_][a-zA-Z0-9._-]{0,127}
 *
 * (This is based on the regular expression given in v1.1.0 of the OCI
 * distribution specification. It also matches the description given in
 * the documentation for the "docker tag" command.)
 */
class repository_tag {
private:
	/** The value of this tag, as a string. */
	std::string _value;
public:
	/** Construct tag from character string
	 * @param value the tag as a std::string_view
	 */
	explicit repository_tag(std::string_view value);

	operator const std::string&() const {
		return _value;
	}

	bool operator==(const repository_tag&) const = default;
	auto operator<=>(const repository_tag&) const = default;
};

inline const std::string& to_string(const repository_tag& repotag) {
	return repotag;
}

extern repository_tag default_repository_tag;

}

#endif
