// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cctype>
#include <stdexcept>

#include "hellmouth/artifacts/hex_digest_value.h"
#include "hellmouth/artifacts/sha256.h"

namespace hellmouth::artifacts {

sha256::sha256() {
	crypto_hash_sha256_init(&_state);
}

void sha256::operator()(const void* data, size_t length) {
	crypto_hash_sha256_update(&_state,
		static_cast<const unsigned char*>(data), length);
}

digest sha256::operator()() {
	unsigned char out[crypto_hash_sha256_BYTES];
	crypto_hash_sha256_final(&_state, out);
	hex_digest_value value(out, sizeof(out));
	return digest(name(), value);
}

const algorithm_name& sha256::name() const {
	static algorithm_name _name("sha256");
	return _name;
}

}
