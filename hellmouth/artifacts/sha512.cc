// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cctype>
#include <stdexcept>

#include "hellmouth/artifacts/hex_digest_value.h"
#include "hellmouth/artifacts/sha512.h"

namespace hellmouth::artifacts {

sha512::sha512() {
	crypto_hash_sha512_init(&_state);
}

void sha512::operator()(const void* data, size_t length) {
	crypto_hash_sha512_update(&_state,
		static_cast<const unsigned char*>(data), length);
}

digest sha512::operator()() {
	unsigned char out[crypto_hash_sha512_BYTES];
	crypto_hash_sha512_final(&_state, out);
	hex_digest_value value(out, sizeof(out));
	return digest(name(), value);
}

const algorithm_name& sha512::name() const {
	static algorithm_name _name("sha512");
	return _name;
}

}
