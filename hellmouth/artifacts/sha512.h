// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ARTIFACTS_SHA512
#define HELLMOUTH_ARTIFACTS_SHA512

#include <sodium.h>

#include "hellmouth/artifacts/algorithm.h"

namespace hellmouth::artifacts {

/** A algorithm class for calculating SHA-512 digests. */
class sha512:
	public algorithm {
private:
	/** The internal state of the algorithm. */
	struct crypto_hash_sha512_state _state;
public:
	/** Construct object for calculating SHA-512 digests. */
	sha512();

	void operator()(const void* data, size_t length) override;
	digest operator()() override;
	const algorithm_name& name() const override;
};

}

#endif
