// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/resources/container.h"

namespace hellmouth::resources {

container::container(resource* parent, resource_name name, spec_type spec):
	resource(parent, std::move(name), std::move(spec)),
	_restart_policy(spec.at_or("restart", false)) {}

void container::state(state_type state) {
	_state = state;
}

std::filesystem::path container::make_pathname(const std::string& ctrname) {
	static std::filesystem::path base_pathname =
		std::filesystem::path(DATADIR) / PKGNAME / "containers";
	std::filesystem::path pathname = base_pathname / ctrname;
	create_directories(pathname);
	return pathname;
}

std::filesystem::path container::make_c2_pathname(
	const std::string& ctrname) {

	return make_pathname(ctrname) / "c2";
}

std::string to_string(container::state_type state) {
	switch (state) {
	case container::state_type::idle:
		return "idle";
	case container::state_type::running:
		return "running";
	case container::state_type::terminating:
		return "terminating";
	case container::state_type::killing:
		return "killing";
	default:
		return "invalid";
	}
}

}
