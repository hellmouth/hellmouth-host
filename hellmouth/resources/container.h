// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_RESOURCES_CONTAINER
#define HELLMOUTH_RESOURCES_CONTAINER

#include <filesystem>

#include "hellmouth/resources/restart_policy.h"
#include "hellmouth/resources/resource.h"

namespace hellmouth::resources {

/** A resource class to represent a container. */
class container:
	public resource {
public:
	static constexpr std::string spec_key = "containers";
	static constexpr std::string driver_key = "container";

	/** An enumeration to represent the state of a container.
	 * Note that there may be a delay between the container
	 * terminating, and this fact being reflected in its reported
	 * state.
	 */
	enum class state_type {
		/** Indicate that the container has either not been
		 * started or is known to have terminated. */
		idle,
		/** Indicate that the container has been started,
		 * is not known to have terminated, and has not been
		 * instructed to terminate by this class. */
		running,
		/** Indicate that the container has been instructed to
		 * terminate by this class, that it is not yet known to
		 * have terminated, and that termination has progressed as
		 * far as sending a SIGTERM. */
		terminating,
		/** Indicate that the container has been instructed to
		 * terminate by this class, that it is not yet known to
		 * have terminated, and that termination has progressed as
		 * as far as sending a SIGKILL. */
		killing
	};

	/** Report that a container could not be mounted. */
	class mount_error;
private:
	/** The current state of this container. */
	state_type _state = state_type::idle;

	/** The restart policy for this container. */
	restart_policy _restart_policy;
protected:
	/** Update the state of this container.
	 * @param state the new state
	 */
	virtual void state(state_type state);
public:
	/** Construct container.
         * @param parent the parent of this container, or 0 if none
	 * @param name the name of this container
	 * @param spec the specification for this container
	 */
	container(resource* parent, resource_name name, spec_type spec);

	/** Get the state of this container.
	 * @return the current state
	 */
	state_type state() const {
		return _state;
	}

	/** Determine whether container is restartable.
         * @param info the cause of the most recent termination
         * @param uptime the time from start to termination,
         *  in milliseconds
         * @return the required restart behaviour
	 */
	restart_policy::restart_behaviour restartable(
		const siginfo_t& info, unsigned long uptime) {

		return _restart_policy(info, uptime);
	}

	/** Get a file descriptor for monitoring this container
	 * The descriptor returned should not be used for any purpose other
	 * than determining whether the container has terminated, which
	 * should be done by testing whether it is readable. Note that:
	 *
	 * - Although the descriptor will typically be a pidfd, this is not
	 *   guaranteed, and operations specific to a pidfd should not be
	 *   attempted.
	 * - Being readable does not guarantee that anything can be read.
	 * - Non-const member functions should be presumed to invalidate
	 *   the result. Recommended practice is to request a fresh value
	 *   immediately prior to use.
	 *
	 * @return the file descriptor, or -1 if not available
	 */
	virtual int pollfd() const = 0;

	/** Start this container.
	 * Once this function has been called, it is an error to call it
	 * again unless the wait function has been called more recently.
	 */
	virtual void start() = 0;

	/** Send a signal to this container.
	 * @param sig the signal to be sent
	 */
	virtual void kill(int sig) = 0;

	/** Wait for this container to terminate.
	 * If the start function has been called, this function must be
	 * called exactly once prior to calling either the start function
	 * again or the destructor.
	 */
	virtual siginfo_t wait() = 0;

	/** Make pathname for storing container-related data.
	 * The sub-path "c2" is reserved for use by the UNIX-domain C2 socket
	 * provided by the container daemon. Other usage is at the discretion
	 * of the container driver.
	 *
	 * @param ctrname the name of the container
	 * @return the corresponding pathname
	 */
	static std::filesystem::path make_pathname(const std::string& ctrname);

	/** Make pathname for UNIX-domain C2 socket.
	 * @param ctrname the name of the container
	 * @return the corresponding C2 pathname
	 */
	static std::filesystem::path make_c2_pathname(
		const std::string& ctrname);
};

std::string to_string(container::state_type state);

class container::mount_error:
	public std::runtime_error {
public:
	/** Construct mount error.
	 * @param msg a message describing the error
	 */
	mount_error(const std::string& msg):
		std::runtime_error(msg) {}
};

}

#endif
