// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_RESOURCES_HOST
#define HELLMOUTH_RESOURCES_HOST

#include "hellmouth/resources/resource.h"

namespace hellmouth::resources {

/** A resource class to represent a host within a HELLMOUTH realm. */
class host:
	public resource {
public:
	static constexpr std::string spec_key = "hosts";
	static constexpr std::string driver_key = "host";

	/** Construct host.
	 * @param parent the parent of this host
	 * @param name the name of this host
	 * @param spec the specification for this host
	 */
	host(resource* parent, resource_name name, spec_type spec):
		resource(parent, std::move(name), std::move(spec)) {}
};

}

#endif
