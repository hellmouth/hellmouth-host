// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_RESOURCES_REALM
#define HELLMOUTH_RESOURCES_REALM

#include "hellmouth/resources/resource.h"

namespace hellmouth::resources {

/** A resource class to represent a HELLMOUTH realm.
 * Realms do not currently provide any substantive behaviour beyond
 * the generic ability of a resource to act as a container for other
 * resources.
 */
class realm:
	public resource {
public:
	static constexpr std::string spec_key = "realms";
	static constexpr std::string driver_key = "realm";

	/** Construct realm.
	 * @param parent the parent of this realm, or 0 if none
	 * @param name the name of this realm
	 * @param spec the specification for this realm
	 */
	realm(resource* parent, resource_name name, spec_type spec):
		resource(parent, std::move(name), std::move(spec)) {}
};

}

#endif
