// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>
#include <regex>

#include <dlfcn.h>

#include "hellmouth/resources/resource.h"

namespace hellmouth::resources {

class resource::resource_not_found_error:
	public std::runtime_error {
public:
	resource_not_found_error(const std::string& name):
		runtime_error(name + " not found") {}
};

resource* resource::_get(const basic_resource_traits& traits,
	const std::string& name) {

	resource_key key(traits, name);
	std::unique_ptr<resource>& entry = _children[key];
	if (!entry) {
		const auto& cspec = _spec
			.at(traits.spec_key())
			.at(to_string(name));
		entry = make_resource(traits, this, name, cspec);
	}
	return entry.get();
}

resource& resource::_find(const basic_resource_traits& traits,
	const std::string& name) {

	resource* parent = this;
	while (parent) {
		const auto& spec1 = parent->_spec;
		if (spec1.contains(traits.spec_key())) {
			const auto& spec2 = spec1.at(traits.spec_key());
			if (spec2.contains(name)) {
				return *parent->_get(traits, name);
			}
		}
		parent = parent->parent();
	}
	throw resource_not_found_error(name);
}

typedef std::unique_ptr<resource> driver_type(resource* parent,
	resource_name name, resource::spec_type spec);

/** Load the driver for a given type and driver name.
 * @param traits a set of traits for the required resource type
 * @param drvname the driver name
 * @return the driver
 */
static driver_type* load_driver(
	const basic_resource_traits& traits, const std::string& drvname) {

	static const std::regex drvname_pattern(
		R"([a-z][a-z0-9-]{0,62})");
	if (!std::regex_match(drvname, drvname_pattern)) {
		throw std::domain_error("invalid driver name: " + drvname);
	}

	std::string pathname = std::string(LIBEXECDIR "/" PKGNAME "/drivers/")
		+ traits.driver_key() + "/" + drvname + ".so";
	void* dlh = dlopen(pathname.c_str(), RTLD_NOW);
	if (!dlh) {
		throw std::runtime_error(
			std::string("failed to load driver: ") + dlerror());
	}

	driver_type* driver = reinterpret_cast<driver_type*>(
		dlsym(dlh, "driver"));
	if (!driver) {
		throw std::runtime_error(
			"driver function missing from shared library");
	}
	return driver;
}

std::unique_ptr<resource> make_resource(
	const basic_resource_traits& traits, resource* parent,
	const std::string& name, const resource::spec_type& spec) {

	std::string drvname = "default";
	if (spec.contains("driver")) {
		drvname = spec.at("driver");
	}
	driver_type* driver = load_driver(traits, drvname);
	return driver(parent, name, spec);
}

}
