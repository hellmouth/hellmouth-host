// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_RESOURCES_RESOURCE
#define HELLMOUTH_RESOURCES_RESOURCE

#include <memory>
#include <map>
#include <string>

#include "hellmouth/data/any.h"
#include "hellmouth/resources/resource_name.h"
#include "hellmouth/resources/resource_traits.h"

namespace hellmouth::resources {

/** A base class to represent a resource.
 * A resource is an entity which can be instantisated by declaring it as
 * part of the cluster specification. Examples include containers, pods,
 * volumes and networks.
 */
class resource {
public:
	/** The type of a resource specification. */
	typedef data::any spec_type;
private:
	/** The parent of this resource, or 0 if none. */
	resource* _parent;

	/** The name of this resource. */
	resource_name _name;

	/** The specification for this resource. */
	spec_type _spec;

	/** A type for uniquely identifying direct children of a resource. */
	struct resource_key {
		/** The key used to identify the resource type in the
		 * specification. */
		std::string spec_key;

		/** The name of the resource. */
		resource_name name;

		/** Construct resource key for given traits and name.
		 * @param traits the required resource traits
		 * @param name the required resource name
		 */
		resource_key(const basic_resource_traits& traits,
			const std::string& name):
			spec_key(traits.spec_key()), name(name) {}

		bool operator==(const resource_key&) const = default;
		auto operator<=>(const resource_key&) const = default;
	};

	/** Instantiated direct children of this resource.
	 * The absence of a child from this mapping implies only that it has not
	 * yet been instantiated. If a requested entry is missing, an attempt
	 * should be made to instantiate it using _make then add it to the
	 * mapping.
	 */
	std::map<resource_key, std::unique_ptr<resource>> _children;

	/** Get the resource with a given name and key.
	 * The requested resource must be a direct child of this resource.
	 *
	 * This function will reuse an existing instance of the resource from
	 * _children where possible, or make a new instance if necessary.
	 *
	 * Ownership of the requested resource is retained by its immediate
	 * parent (which will be this resource).
	 *
	 * @param traits a set of traits for the required resource type
	 * @param name the resource name
	 * @return the resource
	 * @throws resource_not_found_error if not found
	 */
	resource* _get(const basic_resource_traits& traits,
		const std::string& name);

	/** Find the resource for a given name and key.
	 * The result must be either a child of this resource or of one of
	 * its ancestors. If there are multiple matching resources, the one
	 * most closely related to this one is returned.
	 *
	 * Ownership of the requested resource is retained by its immediate
	 * parent.
	 *
	 * @param traits a set of traits for the required resource type
	 * @param name the resource name
	 * @return the resource
	 * @throws resource_not_found_error if not found
	 */
	resource& _find(const basic_resource_traits& traits,
		const std::string& name);
public:
	/** Report that a requested resource was not found. */
	class resource_not_found_error;

	/** Construct resource.
	 * @param parent the parent of this resource, or 0 if none
	 * @param name the name of this resource
	 * @param spec the specification for this resource
	 */
	resource(resource* parent, std::string name, spec_type spec):
		_parent(parent),
		_name(std::move(name)),
		_spec(std::move(spec)) {}

	virtual ~resource() = default;

	/** Get the parent of this resource.
	 * @return the parent, or 0 if none
	 */
	resource* parent() const {
		return _parent;
	}

	/** Get the name of this resource.
	 * @return the name
	 */
	const std::string& name() const {
		return _name;
	}

	/** Get the specification for this resource.
	 * @return the specification
	 */
	const spec_type& spec() const {
		return _spec;
	}

	/** Find the resource with the given type and name.
	 * The result must be either a child of this resource or of one of
	 * its ancestors. If there are multiple matching resources, the one
	 * most closely related to this one is returned.
	 *
	 * Ownership of the requested resource is retained by its immediate
	 * parent.
	 *
	 * @param the resource name
	 * @return the resource specification
	 * @throws resource_not_found_error if not found
	 */
	template<class T>
	T& find(const std::string& name) {
		return dynamic_cast<T&>(_find(resource_traits<T>(), name));
	}
};

/** Make resource from its resource traits and specification.
 * @param traits a set of traits for the required resource type
 * @param parent the parent of this resource, or 0 if none
 * @param name the resource name
 * @param spec the resource specification
 */
std::unique_ptr<resource> make_resource(
	const basic_resource_traits& traits, resource* parent,
	const std::string& name, const resource::spec_type& spec);

/** Make standalone resource from its type and specification.
 * @param name the resource name
 * @param spec the resource specification
 */
template<class T>
std::unique_ptr<T> make_resource(const std::string& name,
	const resource::spec_type& spec) {

	auto res = make_resource(resource_traits<T>(), 0, name, spec);
	T& tres = dynamic_cast<T&>(*res.get());
	res.release();
	return std::unique_ptr<T>(&tres);
}

}

#endif
