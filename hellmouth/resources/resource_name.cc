// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>
#include <regex>

#include "hellmouth/resources/resource_name.h"

namespace hellmouth::resources {

resource_name::resource_name(std::string value):
	_value(std::move(value)) {

	static const std::regex name_pattern(
		R"([a-zA-Z][a-zA-Z0-9_]{0,62})");
	if (!std::regex_match(_value, name_pattern)) {
		throw std::invalid_argument("invalid resource name");
	}
}

}
