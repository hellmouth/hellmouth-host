// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_RESOURCES_RESOURCE_NAME
#define HELLMOUTH_RESOURCES_RESOURCE_NAME

#include <string>

namespace hellmouth::resources {

/** A class to represent a validated resource name.
 * The syntax for a resource name has not been finalised, and to keep
 * options open it is currently restricted to the following syntax:
 *
 * resource-name ::= [a-zA-Z][a-zA-Z0-9_]{0,62}
 */
class resource_name {
private:
	/** The value of this resource name, as a string. */
	std::string _value;
public:
	/** Construct resource name from character string
	 * @param value the name as a std::string
	 */
	resource_name(std::string);

	operator const std::string&() const {
		return _value;
	}

	bool operator==(const resource_name&) const = default;
	auto operator<=>(const resource_name&) const = default;
};

inline const std::string& to_string(const resource_name& resname) {
	return resname;
}

}

#endif
