// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_RESOURCES_RESOURCE_TRAITS
#define HELLMOUTH_RESOURCES_RESOURCE_TRAITS

#include <string>

namespace hellmouth::resources {

/** A base class to represent the properties of a resource type. */
class basic_resource_traits {
public:
	virtual ~basic_resource_traits() = default;

	/** Get the object key for use in specifications.
	 * This should normally be the plural form of the abstract resource
	 * type, for example "volumes" or "nerworks".
	 *
	 * @return the key
	 */
	virtual const std::string& spec_key() const = 0;

	/** Get the object key for use in drivers.
	 * This should normally be the singular form of the abstract resource
	 * type, for example "volume" or "nerwork".
	 * @return the key
	 */
	virtual const std::string& driver_key() const = 0;
};

/** A template class to represent the properties of a resource type. */
template<class T>
class resource_traits:
	public basic_resource_traits {
public:
	/** Get the object key for use in specifications.
	 * This should normally be the plural form of the abstract resource
	 * type, for example "volumes" or "nerworks".
	 *
	 * @return the key
	 */
	const std::string& spec_key() const override {
		return T::spec_key;
	}

	/** Get the object key for use in drivers.
	 * This should normally be the singular form of the abstract resource
	 * type, for example "volume" or "nerwork".
	 *
	 * @return the key
	 */
	const std::string& driver_key() const override {
		return T::driver_key;
	}
};

}

#endif
