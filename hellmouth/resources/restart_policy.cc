// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <map>

#include "hellmouth/resources/restart_policy.h"

namespace hellmouth::resources {

static std::map<std::string, restart_policy::condition_type> conditions = {
	{"always", restart_policy::condition_type::always},
	{"on-failure", restart_policy::condition_type::on_failure},
	{"never", restart_policy::condition_type::never}};

restart_policy::restart_policy(const data::any& spec) {
	if (spec.is_object()) {
		_condition = conditions.at(spec.at("condition"));
		if (spec.contains("fail-after")) {
			_fail_after = spec.at("fail-after");
		}
		if (spec.contains("recreate-after")) {
			_recreate_after = spec.at("recreate-after");
		}
		if (spec.contains("initial-wait")) {
			_initial_wait = spec.at("initial-wait");
		}
		if (spec.contains("maximum-wait")) {
			_maximum_wait = spec.at("maximum-wait");
		}
		if (spec.contains("stable_after")) {
			_stable_after = spec.at("stable-after");
		}
	} else {
		_condition = (spec) ?
			condition_type::always :
			condition_type::never;
	}
}

bool restart_policy::condition_satisfied(const siginfo_t& info) const {
	switch (_condition) {
	case condition_type::always:
		return true;
	case condition_type::on_failure:
		return !(info.si_code == CLD_EXITED && info.si_status == 0);
	default:
		return false;
	}
}

restart_policy::restart_behaviour restart_policy::operator()(
	const siginfo_t& info, long uptime) {

	// Determine whether the container achieved stable operation.
	bool stable = (uptime >= _stable_after);
	if (stable) {
		// If stable then reset both the number of consecutive
		// unsuccessful restarts and the required wait between
		// container starts.
		_restarts = 0;
		_wait = 0;
	} else {
		// Otherwise, increment the number of consecutive
		// unsuccessful restarts.
		_restarts += 1;
	}

	// If the specified restart condition has not been specified
	// then do not restart the container.
	if (!condition_satisfied(info)) {
		return restart_behaviour();
	}

	// Determine whether the container should be recreated.
	//


	bool recreate = false;
	if (_recreate_after >= 0) {
		if (_restarts % (_recreate_after + 1) == 0) {
			recreate = true;
		}
	}

	// The required wait is, in effect, post-incremented.
	// Therefore, keep a copy of its current value for later return.
	long wait = _wait;

	// The required wait before the first restart is always zero.
	// Before the second restart it is set by _initial_wait.
	// Before following restarts it backs off exponentially, up to
	// the maximum set by _maximum_wait.
	if (_wait == 0) {
		_wait = _initial_wait;
	} else {
		if (_wait < _maximum_wait / 2) {
			_wait *= 2;
		} else {
			_wait = _maximum_wait;
		}
	}

	// Calculate the required delay before restarting.
	long delay = (wait > uptime) ? 0 : (wait - uptime);
	return restart_behaviour(recreate, delay);
}

}
