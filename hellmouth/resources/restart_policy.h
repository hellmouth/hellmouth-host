// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_RESOURCES_RESTART_POLICY
#define HELLMOUTH_RESOURCES_RESTART_POLICY

#include <sys/wait.h>

#include "hellmouth/data/any.h"

namespace hellmouth::resources {

/** A structure for deciding whether a container should be restarted.
 * There are two trivial policies for which can be specified by means of
 * a shorthand notation:
 *
 * - never (represented by the value null or false), and
 * - always (represented by the value true).
 *
 * Alternatively, a more detailed specification (represented by a
 * data::object) can be given.
 */
struct restart_policy {
public:
	/** An enumeration for specifying the circumstances in which
	 * a container is potentially eligible to be restarted. */
	enum class condition_type {
		/** Always restart. */
		always,
		/** Restart only if terminated by a signal, or normally
		 * but with a non-zero exit code. */
		on_failure,
		/** Never restart. */
		never
	};

	/** A structure for specifying the required restart behaviour
	 * for a container which results from applying a policy. */
	struct restart_behaviour {
		/** True if the container should be restarted,
		 * otherwise false. */
		bool restart = false;

		/** True if the container should be recreated,
		 * otherwise false. */
		bool recreate = false;

		/** The number of milliseconds to wait before restarting. */
		unsigned long delay = 0;

		/** Specify that the container should not restart. */
		restart_behaviour() = default;

		/** Specify that the container should restart.
		 * @param recreate true if it should also be recreated,
		 *  otherwise false
		 * @param delay the number of milliseconds to wait before
		 *  restarting
		 */
		restart_behaviour(bool recreate, unsigned long delay):
			restart(true), recreate(recreate), delay(delay) {}
	};
private:
	/** The restart condition. */
	condition_type _condition = condition_type::never;

	/** The number of consecutive unsuccessful restarts after which to
	 * give up, or -1 to continue restarting indefinitely. */
	int _fail_after = -1;

	/** The number of consecutive unsuccessful restarts after which to
	 * recreate the container, as opposed to merely restarting it. */
	int _recreate_after = 3;

	/** The initial required wait between container starts. */
	long _initial_wait = 1000;

	/** The maximum required wait between container starts. */
	long _maximum_wait = 300000;

	/** The period after which a container run is considered stable. */
	long _stable_after = 10000;

	/** The number of consecutive unsuccessful container runs. */
	long _restarts = 0;

	/** The current required wait between container starts. */
	long _wait = 0;

	/** Test whether the restart condition has been satisfied.
	 * @param info the outcome the most recent container run.
	 */
	bool condition_satisfied(const siginfo_t& info) const;
public:
	/** Construct default restart policy (never). */
	restart_policy() = default;

	/** Construct restart policy from specification.
	 * @param spec the specification
	 */
	restart_policy(const data::any& spec);

	/** Decide behaviour following termination.
	 * If the uptime cannot be represented by the specified type
	 * then the value should saturate.
	 *
	 * @param info the cause of the most recent termination
	 * @param uptime the time from start to termination,
	 *  in milliseconds
	 * @return the required restart behaviour
	 */
	restart_behaviour operator()(const siginfo_t& info, long uptime);
};

}

#endif
