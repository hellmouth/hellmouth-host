// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_RESOURCES_VOLUME
#define HELLMOUTH_RESOURCES_VOLUME

#include <stdexcept>
#include <string>

#include "hellmouth/resources/resource.h"

namespace hellmouth::resources {

/** A resource class to represent a data storage volume. */
class volume:
	public resource {
public:
	static constexpr std::string spec_key = "volumes";
	static constexpr std::string driver_key = "volume";

	/** Report that a volume could not be mounted. */
	class mount_error;

	/** Construct volume.
	 * @param parent the parent of this volume, or 0 if none
	 * @param name the volume name
	 * @param spec the volume specification
	 */
	volume(resource* parent, std::string name, spec_type spec):
		resource(parent, std::move(name), std::move(spec)) {}

	/** Mount this volume.
	 * The caller is responsible for ensuring that:
	 *
	 * - the mount point exists;
	 * - that it is not already mounted; and
	 * - that this is the volume named in the mount point specification.
	 *
	 * @param pathname the pathname of the mount point (as viewed from
	 *  outside the container)
	 * @param mountspec the mount point specification
	 * @throws mount_error if the volume could not be mounted
	 */
	virtual void mount(const std::string& pathname,
		const spec_type& mountspec) = 0;
};

class volume::mount_error:
	public std::runtime_error {
public:
	/** Construct mount error.
	 * @param msg a message describing the error
	 */
	mount_error(const std::string& msg):
		std::runtime_error(msg) {}
};

}

#endif
