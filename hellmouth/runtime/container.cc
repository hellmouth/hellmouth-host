// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include "hellmouth/runtime/container.h"

namespace hellmouth::runtime {

container::container(event::manager& em, std::string name):
	_name(std::move(name)),
	_em(&em),
	_trigger(*_em, [&]{ apply(); }) {}

void container::start() {
	if (_ctr && _ctr->state() != state_type::idle) {
		throw std::runtime_error(
			"Cannot start container (not in idle state)");
	}

	if (!_ctr) {
		_ctr = resources::make_resource<resources::container>(
			_name, _spec);
	}
	_ctr->start();

	auto action = [this]{
		_child_event = event::file_descriptor();
		_kill_event = event::timeout();
		siginfo_t outcome = _ctr->wait();
		auto restart_action = _ctr->restartable(outcome, 10); // Need correct uptime
		_restartable = restart_action.restart;
		_delayed = _restartable && restart_action.delay != 0;
		if (_delayed) {
			auto action2 = [this]{
				_delayed = false;
				apply();
			};
			_restart_event = event::timeout(*_em, action2, restart_action.delay);
		}
		apply();
	};
	_child_event = event::file_descriptor(*_em, action, _ctr->pollfd());
}

void container::stop() {
	if (_ctr && _ctr->state() == state_type::running) {
		_ctr->kill(SIGTERM);
		auto action = [this]{
			_kill_event = event::timeout();
			_ctr->kill(SIGKILL);
		};
		_kill_event = event::timeout(*_em, action, 10000);
	}
}

bool container::is_running() const {
	return _ctr && _ctr->state() == state_type::running;
}

void container::apply() {
	std::lock_guard config_lock(_mutex);
	bool want_running = _spec.contains("running") && _spec.at("running");
	if (want_running && !is_running() && _restartable && !_delayed) {
		start();
	} else if (is_running() && !want_running) {
		stop();
	}
}

void container::report_status() {
	if (!_ctr) {
		return;
	}

	state_type state = _ctr->state();
	if (_state == state) {
		return;
	}
	_state = state;

	data::any status;
	status["state"] = to_string(_state);
	report(status);
}

void container::configure(const data::any& config) {
	std::lock_guard lock(_mutex);
	stop();
	_spec = config;
	_restartable = true;
	_trigger.notify();
}

}
