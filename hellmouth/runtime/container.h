// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_RUNTIME_CONTAINER
#define HELLMOUTH_RUNTIME_CONTAINER

#include <functional>
#include <filesystem>
#include <iostream>

#include <signal.h>

#include "hellmouth/json/encoder.h"

#include "hellmouth/event/manager.h"
#include "hellmouth/event/timeout.h"
#include "hellmouth/event/file_descriptor.h"
#include "hellmouth/event/notification.h"
#include "hellmouth/resources/container.h"
#include "hellmouth/c2/agent.h"
#include "hellmouth/c2/server.h"

namespace hellmouth::runtime {

/** A class for managing a container. */
class container:
	public c2::agent {
public:
	typedef resources::container::state_type state_type;
private:
	/** The name of this container. */
	std::string _name;

	/** A mutex governing access to _spec. */
	std::mutex _mutex;

	/** The container specification. */
	data::any _spec;

	/** The container built from the specification. */
	std::unique_ptr<resources::container> _ctr;

	/** True to allow container to start/restart, otherwise false. */
	bool _restartable = true;

	/** True if in wait period between termination and restart. */
	bool _delayed = false;

	/** The last known state of the container. */
	state_type _state = state_type::idle;

	/** An event manager for detecting events which this class has a
	 * need to act upon. */
	event::manager *_em;

	/** An event for indicating that the configuration should be
	 * re-applied. */
	event::notification _trigger;

	/** An event for determining when a SIGKILL should be sent to the
	 * container.
	 *
	 * This should be armed on sending the initial SIGTERM, and disarmed
	 * if the container terminates (as it should) without the need for a
	 * SIGKILL to be sent.
	 */
	event::timeout _kill_event;

	/** An event for determining when the container has terminated.
	 * A pidfd is used for this purpose in preference to a signalfd,
	 * since it is easier to use and has cleaner semantics.
	 *
	 * Note that this event must be recreated whenever the value of the
	 * process descriptor changes (including when it becomes empty).
	 */
	event::file_descriptor _child_event;

	/** An event for determining when the container should be restarted. */
	event::timeout _restart_event;

	/** Check whether container is in the running state.
	 * This does not include the terminating or killing states.
	 *
	 * @return true if running, otherwise false
	 */
	bool is_running() const;

	/** Start the container. */
	void start();

	/** Stop the child process. */
	void stop();

	/** Apply the configuration to the child process. */
	void apply();
public:
	/** Construct new container.
	 * @param em an event manager for use by the container
	 * @param name the name of the container
	 */
	container(event::manager& em, std::string name);

	/** Set new configuration. */
	void configure(const data::any& config) override;

	/** Report status to clients. */
	void report_status();
};

}

#endif
