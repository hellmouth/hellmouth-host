// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <filesystem>
#include <iostream>

#include "hellmouth/resources/container.h"
#include "hellmouth/event/manager.h"
#include "hellmouth/runtime/container.h"
#include "hellmouth/c2/server.h"

using namespace hellmouth;

/** Print help text.
 * @param out the ostream to which the help text should be written
 */
void write_help(std::ostream& out) {
	out << "Usage: hellmouth container daemon [<options>] name"
		<< std::endl;
	out << std::endl;
	out << "Options:" << std::endl;
	out << std::endl;
	out << "  -h  display this help text then exit" << std::endl;
}

int main(int argc, char* argv[]) {
	sigset_t sigset;
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGPIPE);
	sigprocmask(SIG_BLOCK, &sigset, 0);

	int opt;
	while ((opt = getopt(argc, argv, "h")) != -1) {
		switch (opt) {
		case 'h':
			write_help(std::cout);
			return 0;
		}
	}

	unsigned int errors = 0;
	if (optind >= argc) {
		std::cerr << "Container name not specified"
			<< std::endl;
		errors += 1;
	}
	if (errors) {
		return -1;
	}

	std::string ctrname = argv[optind];
	std::filesystem::path c2_pathname =
		resources::container::make_c2_pathname(ctrname);

	event::manager em;
	runtime::container ctr(em, ctrname);
	c2::server server(ctr, c2_pathname);

	auto accept = [&](){
		server.accept();
	};

	// [TODO]: Fire-and-forget is probably not a good enough
	// solution here, because there may very well be a need for
	// the server to exit at the request of one client while
	// other clients are still connected.
	auto spawn_accept = [&](){
		std::thread th(accept);
		th.detach();
	};

	event::file_descriptor server_event(em, spawn_accept, server.fd());

	while (true) {
		em.poll();
		ctr.report_status();
	}
}
