// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <iostream>

#include "hellmouth/artifacts/artifact_name.h"
#include "hellmouth/artifacts/image_store.h"

using namespace hellmouth;

/** Print help text.
 * @param out the ostream to which the help text should be written
 */
void write_help(std::ostream& out) {
	out << "Usage: hellmouth image list [<options>]"
		<< std::endl;
	out << std::endl;
	out << "Options:" << std::endl;
	out << std::endl;
	out << "  -h  display this help text then exit" << std::endl;
}

int main(int argc, char* argv[]) {
	int opt;
	while ((opt = getopt(argc, argv, "h")) != -1) {
		switch (opt) {
		case 'h':
			write_help(std::cout);
			return 0;
		}
	}

	artifacts::image_store imgstore("/var/lib/hellmouth/images");
	for (const auto& imgname : imgstore) {
		std::cout << to_string(imgname) << "\n";
	}
}
