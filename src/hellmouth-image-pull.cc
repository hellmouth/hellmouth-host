// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <iostream>

#include "hellmouth/artifacts/artifact_name.h"
#include "hellmouth/artifacts/image_store.h"

using namespace hellmouth;

/** Print help text.
 * @param out the ostream to which the help text should be written
 */
void write_help(std::ostream& out) {
	out << "Usage: hellmouth image pull [<options>] <image-name> ..."
		<< std::endl;
	out << std::endl;
	out << "Options:" << std::endl;
	out << std::endl;
	out << "  -h  display this help text then exit" << std::endl;
	out << "  -i  fetch using plain HTTP (not HTTP+TLS)" << std::endl;
}

int main(int argc, char* argv[]) {
	bool insecure = false;

	int opt;
	while ((opt = getopt(argc, argv, "hi")) != -1) {
		switch (opt) {
		case 'h':
			write_help(std::cout);
			return 0;
		case 'i':
			insecure = true;
			break;
		}
	}

	unsigned int errors = 0;
	if (optind == argc) {
		std::cerr << "No image names specified"
			<< std::endl;
		errors += 1;
	}
	if (errors) {
		return -1;
	}

	artifacts::image_store imgstore("/var/lib/hellmouth/images");
	for (int i = optind; i != argc; ++i) {
		artifacts::artifact_name imgname(argv[i]);
		imgname = imgname.canonicalise();
		imgstore[imgname].pull(!insecure);
		std::cout << to_string(imgname) << "\n";
	}
}
